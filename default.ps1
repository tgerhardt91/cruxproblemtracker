Framework "4.5.1"

properties {
    $baseDir = resolve-path .

    $dataFolder = "$baseDir\data"

    $buildFolder = "$baseDir\build"

    $roundhouseExec = "$buildFolder\roundhouse\rh.exe"

    $databaseServer = "(local)\sqlexpress2014"
    $problemTrackerDbName = "ProblemTracker"
}

task dbonly -depends RebuildDatabase
task dbupdate -depends UpdateDatabase

task UpdateDatabase {
    $dbFileDir = "$dataFolder\database\roundhouse"
    $versionFile = "$dbFileDir\_BuildInfo.xml"
    $enviornment = "LOCAL"

    Exec {
        &$roundhouseExec /d=$problemTrackerDbName /f=$dbFileDir /s=$databaseServer /vf=$versionFile /vx='//buildInfo/version' /env=$enviornment /simple /silent
    }
}

task RebuildDatabase {
    $dbFileDir = "$dataFolder\database\roundhouse"
    $versionFile = "$dbFileDir\_BuildInfo.xml"
    $enviornment = "LOCAL"

    Exec {
        &$roundhouseExec /d=$problemTrackerDbName /f=$dbFileDir /s=$databaseServer /vf=$versionFile /vx='//buildInfo/version' /env=$enviornment /drop /silent
    }

    Exec {
        &$roundhouseExec /d=$problemTrackerDbName /f=$dbFileDir /s=$databaseServer /vf=$versionFile /vx='//buildInfo/version' /env=$enviornment /simple /silent
    }
}