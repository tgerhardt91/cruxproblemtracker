﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Admin;
using Core.ProblemManagement;
using Core.ProblemManagement.Objects;

namespace Infrastructure.Admin
{
    public class AdminUiAdapter
    {
        private readonly IProblemRepository _problemRepository;
        private readonly ISetRepository _setRepository;

        public AdminUiAdapter(
            IProblemRepository problemRepository,
            ISetRepository setRepository
        )
        {
            _problemRepository = problemRepository;
            _setRepository = setRepository;
        }

        public AddProblemResponse AddProblem(Problem problem)
        {
            try
            {
                _problemRepository.InsertProblem(problem);
            }
            catch (Exception ex)
            {
                return new AddProblemResponse
                {
                    ProblemAdded = false,
                    Message = ex.Message
                };
            }

            return new AddProblemResponse
            {
                ProblemAdded = true,
                Message = "Problem Added"
            };
        }

        public AddSetResponse AddSet(Set set)
        {
            try
            {
                _setRepository.InsertSet(set);
            }
            catch (Exception ex)
            {
                return new AddSetResponse
                {
                    SetAdded = false,
                    Message = ex.Message
                };
            }
            
            return new AddSetResponse
            {
                SetAdded = true,
                Message = "Set Added"
            };
        }
    }
}
