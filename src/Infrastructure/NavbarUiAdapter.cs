﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement;
using Core.ProblemManagement.Objects;

namespace Infrastructure
{
    public class NavbarUiAdapter
    {
        private readonly ISetRepository _setRepository;

        public NavbarUiAdapter(
        ISetRepository setRepository)
        {
            _setRepository = setRepository;
        }

        public List<Set> GetAllSets()
        {
            return _setRepository.GetAllSets().ToList();
        }

        public Set GetMostRecentSet()
        {
            var setsOrderedByDate = _setRepository.GetAllSets().ToList().
                OrderByDescending(x => x.SetDate);

            return setsOrderedByDate.FirstOrDefault();
        }
    }
}
