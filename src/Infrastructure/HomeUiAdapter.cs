﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement;
using Core.ProblemManagement.Objects;

namespace Infrastructure
{
    public class HomeUiAdapter
    {
        private readonly ISetRepository _setRepository;
        private readonly IProblemRepository _problemRepository;

        public HomeUiAdapter(
            ISetRepository setRepository,
            IProblemRepository problemRepository
        )
        {
            _setRepository = setRepository;
            _problemRepository = problemRepository;
        }

        public List<Set> GetSets()
        {
            return _setRepository.GetAllSets().OrderByDescending(s => s.SetDate).ToList();
        }

        public List<Problem> GetProblemsFromSet(Guid setId)
        {
            return _problemRepository.GetProblemsBySetId(setId).ToList();
        }
    }
}
