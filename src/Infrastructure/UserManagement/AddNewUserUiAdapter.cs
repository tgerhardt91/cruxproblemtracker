﻿using System;
using Core.UserManagement;
using Core.UserManagement.Objects;

namespace Infrastructure.UserManagement
{
    public class AddNewUserUiAdapter
    {
        private readonly IUserLoginRepository _userLoginRepository;
        private readonly IUserRepository _userRepository;

        public AddNewUserUiAdapter(
            IUserRepository userRepository,
            IUserLoginRepository userLoginRepository)
        {
            _userRepository = userRepository;
            _userLoginRepository = userLoginRepository;
        }

        public AddNewUserRecord AddNewUser(string userName, string password, string email, string firstName, string lastName)
        {
            var addUserRecord = new AddNewUserRecord();

            var response = GetNewUserValidationResponse(userName, email, password);

            if (!IsValidNewUser(response))
            {
                addUserRecord.NewUserAdded = false;
                addUserRecord.Message = CreateValidationFailureMessage(response);

                return addUserRecord;
            }

            var userId = Guid.NewGuid();

            var newUser = new User
            {
                UserId = userId,
                UserName = userName,
                Email = email,
                IsAdmin = false,
                FirstName = firstName,
                LastName = lastName
            };

            var hasher = new SecurePasswordHasher();

            var newUserLogin = new UserLogin
            {
                UserId = userId,
                Email = email,
                Password = hasher.Hash(password)
            };

            try
            {
                _userRepository.InsertNewUser(newUser);
                _userLoginRepository.InsertNewUserLogin(newUserLogin);
            }
            catch (Exception e)
            {
                addUserRecord.NewUserAdded = false;
                addUserRecord.Message = e.Message;

                return addUserRecord;
            }

            addUserRecord.NewUserAdded = true;
            addUserRecord.Message = "Success";

            return addUserRecord;
        }

        private NewUserValidationResponse GetNewUserValidationResponse(string userName, string email, string password)
        {
            var newUserValidator = new NewUserValidator();

            var existingUsers = _userRepository.GetAllUsers();

            return newUserValidator.ValidateNewUser(userName, email, password, existingUsers);
        }

        private static string CreateValidationFailureMessage(NewUserValidationResponse validationResponse)
        {
            if (!validationResponse.IsEmailUnique)
                return "An account with that email address has already been created";

            if (!validationResponse.IsUserNameUnique)
                return "Username is already taken";

            var badPasswordReason = "";

            if (!validationResponse.DoesPasswordMeetMinimumLength)
                badPasswordReason += "Password must be at least 7 characters long\n";

            if(!validationResponse.DoesPasswordMeetMinimumComplexity)
                badPasswordReason += "Password must contain at least 1 special character, 1 uppercase character and 1 number\n";

            return badPasswordReason;
        }

        private static bool IsValidNewUser(NewUserValidationResponse validationResponse)
        {
            return !(!validationResponse.IsUserNameUnique || !validationResponse.DoesPasswordMeetMinimumComplexity ||
                !validationResponse.DoesPasswordMeetMinimumLength);
        }
    }
}
