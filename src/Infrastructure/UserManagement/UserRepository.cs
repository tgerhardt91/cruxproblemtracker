﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.UserManagement;
using Core.UserManagement.Objects;
using Core.UserManagement.Dtos;

namespace Infrastructure.UserManagement
{
    public class UserRepository : IUserRepository
    {
        private readonly IUserGateway _userGateway;

        public UserRepository(
            IUserGateway userGateway)
        {
            _userGateway = userGateway;
        }

        public void InsertNewUser(User newUser)
        {
            _userGateway.InsertNewUserDto(CreateUserDto(newUser));
        }

        public User GetUserById(Guid userId)
        {
            return CreateUser(_userGateway.GetUserById(userId));
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _userGateway.GetAllUsers().Select(CreateUser).ToList();
        }

        private User CreateUser(UserDto userDto)
        {
            return new User
            {
                UserId = userDto.UserId,
                UserName = userDto.UserName,
                Email = userDto.Email,
                IsAdmin = userDto.IsAdmin,
                FirstName = userDto.FirstName,
                LastName = userDto.LastName
            };
        }

        private UserDto CreateUserDto(User user)
        {
            return new UserDto
            {
                UserId = user.UserId,
                UserName = user.UserName,
                Email = user.Email,
                IsAdmin = user.IsAdmin,
                FirstName = user.FirstName,
                LastName = user.LastName
            };
        }
    }
}
