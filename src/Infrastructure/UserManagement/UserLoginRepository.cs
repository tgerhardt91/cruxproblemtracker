﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.UserManagement;
using Core.UserManagement.Objects;

namespace Infrastructure
{
    public class UserLoginRepository : IUserLoginRepository
    {
        private readonly IUserLoginGateway _userLoginGateway;

        public UserLoginRepository(
            IUserLoginGateway userLoginGateway)
        {
            _userLoginGateway = userLoginGateway;
        }

        public void InsertNewUserLogin(UserLogin newUser)
        {
            _userLoginGateway.InsertUserLoginDto(CreateUserLoginDto(newUser));        
        }

        public UserLogin GetUserLoginById(Guid userId)
        {
            return CreateUserLogin(_userLoginGateway.GetUserLoginByUserId(userId));
        }

        private static UserLogin CreateUserLogin(UserLoginDto dto)
        {
            return new UserLogin
            {
                UserId = dto.UserId,
                Email = dto.Email,
                Password = dto.Password
            };
        }

        private static UserLoginDto CreateUserLoginDto(UserLogin newUserLogin)
        {
            return new UserLoginDto
            {
                UserId = newUserLogin.UserId,
                Email = newUserLogin.Email,
                Password = newUserLogin.Password
            };
        }
    }
}
