﻿using Core.UserManagement;
using Core.UserManagement.Objects;
using System.Linq;

namespace Infrastructure.UserManagement
{
    public class UserLoginUiAdapter
    {
        private readonly IUserLoginRepository _userLoginRepository;
        private readonly IUserRepository _userRepository;

        public UserLoginUiAdapter(IUserLoginRepository userLoginRepository, IUserRepository userRepository)
        {
            _userLoginRepository = userLoginRepository;
            _userRepository = userRepository;
        }

        public UserLoginRecord VerifyEmailAndPassword(string email, string password)
        {
            var allUsers = _userRepository.GetAllUsers();

            User userEmailMatch;
            try
            {
                userEmailMatch = allUsers.SingleOrDefault(user => user.Email == email);
            }
            catch
            {
                return new UserLoginRecord
                {
                    UserLoggedIn = false,
                    Message = "ERROR: There was more than one user with the entered email address"
                };
            }

            if (userEmailMatch != null)
            {
                UserLogin userLoginIdMatch;
                try
                {
                    userLoginIdMatch = _userLoginRepository.GetUserLoginById(userEmailMatch.UserId);
                }
                catch
                {
                    return new UserLoginRecord
                    {
                        UserLoggedIn = false,
                        Message = "No Guid in the UserLogin table matches the Guid from the Users table"
                    };
                }
                var hasher = new SecurePasswordHasher();

                if (hasher.Verify(password, userLoginIdMatch.Password))
                {
                    return new UserLoginRecord
                    {
                        UserLoggedIn = true,
                        FirstName = userEmailMatch.FirstName,
                        Message = "The email and password were successfully verified"
                    };
                }
                else
                {
                    return new UserLoginRecord
                    {
                        UserLoggedIn = false,
                        Message = "ERROR: A matching email was found but the entered password was not correct"
                    };
                }
            }
            else
            {
                return new UserLoginRecord
                {
                    UserLoggedIn = false,
                    Message = "ERROR: No user with that email address was found"
                };
            }
        }
    }
}
