﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement;
using Core.ProblemManagement.Dtos;
using Core.ProblemManagement.Objects;

namespace Infrastructure.ProblemManagement
{
    public class SetRepository : ISetRepository
    {
        private readonly ISetGateway _setGateway;

        public SetRepository(
            ISetGateway setGateway)
        {
            _setGateway = setGateway;
        }

        public void InsertSet(Set set)
        {
            _setGateway.InsertSetDto(CreateSetDto(set));
        }

        public IEnumerable<Set> GetAllSets()
        {
            return _setGateway.GetAllSetDtos().Select(CreateSet);
        }

        private static SetDto CreateSetDto(Set set)
        {
            return new SetDto
            {
                SetId = set.SetId,
                SetDate = set.SetDate,
                SetLocation = set.SetLocation
            };
        }

        private static Set CreateSet(SetDto dto)
        {
            return new Set
            {
                SetId = dto.SetId,
                SetDate = dto.SetDate,
                SetLocation = dto.SetLocation
            };
        }
    }
}
