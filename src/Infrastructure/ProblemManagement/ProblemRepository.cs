﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement;
using Core.ProblemManagement.Objects;

namespace Infrastructure.ProblemManagement
{
    public class ProblemRepository : IProblemRepository
    {
        private readonly IProblemGateway _problemGateway;

        public ProblemRepository(
            IProblemGateway problemGateway)
        {
            _problemGateway = problemGateway;
        }

        public void InsertProblem(Problem problem)
        {
            _problemGateway.InsertProblemDto(CreateProblemDto(problem));
        }

        public IEnumerable<Problem> GetAllProblems()
        {
            return _problemGateway.GetAllProblemDtos().Select(CreateProblem);
        }

        public IEnumerable<Problem> GetProblemsBySetId(Guid setId)
        {
            return _problemGateway.GetBySetId(setId).Select(CreateProblem);
        }

        private static Problem CreateProblem(ProblemDto dto)
        {
            return new Problem
            {
                ProblemId = dto.ProblemId,
                Grade = dto.Grade,
                CommunityGrade = dto.CommunityGrade,
                Color = dto.Color,
                Setter = dto.Setter,
                AssociatedSet = new Set
                {
                    SetId = dto.SetId,
                    SetDate = dto.SetDate,
                    SetLocation = dto.SetLocation
                },
                Tags = dto.Tags
            };
        }

        private static ProblemDto CreateProblemDto(Problem problem)
        {
            return new ProblemDto
            {
                Color = problem.Color,
                CommunityGrade = problem.CommunityGrade,
                Grade = problem.Grade,
                ProblemId = problem.ProblemId,
                Setter = problem.Setter,
                SetId = problem.AssociatedSet.SetId,
                SetDate = problem.AssociatedSet.SetDate,
                SetLocation = problem.AssociatedSet.SetLocation,
                Tags = problem.Tags
            };
        }
    }
}
