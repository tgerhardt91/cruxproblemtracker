﻿using Core.UserManagement;
using StructureMap;

namespace Infrastructure.DependencyResolution
{
    public class DefaultRegistry : Registry
    {
        public DefaultRegistry()
        {
            Scan(scan =>
            {
                scan.WithDefaultConventions();
                scan.TheCallingAssembly();
            });
        }
    }
}
