﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement;
using HybridTests.DependancyResolution;
using Core.UserManagement;

namespace HybridTests
{
    public class DatabaseCleaner
    {
        public static void DeleteAllData()
        {
            var problemGateway = IocHybridWrapper.GetInstance<IProblemGateway>();
            problemGateway.DeleteAllRecords();

            var setGateway = IocHybridWrapper.GetInstance<ISetGateway>();
            setGateway.DeleteAllRecords();

            var userGateway = IocHybridWrapper.GetInstance<IUserGateway>();
            userGateway.DeleteAllRecords();

            var userLoginGateway = IocHybridWrapper.GetInstance<IUserLoginGateway>();
            userLoginGateway.DeleteAllRecords();
        }
    }
}
