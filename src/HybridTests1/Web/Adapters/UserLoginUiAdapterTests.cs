﻿using FluentAssertions;
using HybridTests.DependancyResolution;
using Infrastructure.UserManagement;
using Xunit;

namespace HybridTests.Web.Adapters
{
    public class UserLoginUiAdapterTests
    {
        [Fact]
        public void AddingValidNewUserLoginShouldReturnCorrectAddNewUserRecord()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var adapter = IocHybridWrapper.GetInstance<AddNewUserUiAdapter>();

                    var addNewUserRecord = adapter.AddNewUser("user4", "Passw@rd123", "user4@test.com", "john", "four");

                    addNewUserRecord.NewUserAdded.Should().BeTrue();
                    addNewUserRecord.Message.Should().Be("Success");
                });
        }

        [Fact]
        public void ValidUserLoginShouldReturnCorrectUserLoginRecord()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var newUserAdapter = IocHybridWrapper.GetInstance<AddNewUserUiAdapter>();
                    var userLoginAdapter = IocHybridWrapper.GetInstance<UserLoginUiAdapter>();

                    newUserAdapter.AddNewUser("user1", "Passw@rd123", "user1@test.com", "john", "one");

                    var userLoginRecord = userLoginAdapter.VerifyEmailAndPassword("user1@test.com", "Passw@rd123");

                    userLoginRecord.UserLoggedIn.Should().BeTrue();
                    userLoginRecord.Message.Should().Be("The email and password were successfully verified");
                });
        }

        [Fact]
        public void InvalidUserLoginShouldReturnNoEmailFoundError()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var newUserAdapter = IocHybridWrapper.GetInstance<AddNewUserUiAdapter>();
                    var userLoginAdapter = IocHybridWrapper.GetInstance<UserLoginUiAdapter>();

                    newUserAdapter.AddNewUser("user1", "Passw@rd123", "user1@test.com", "john", "one");

                    var userLoginRecord = userLoginAdapter.VerifyEmailAndPassword("user4@test.com", "Passw@rd1234");

                    userLoginRecord.UserLoggedIn.Should().BeFalse();
                    userLoginRecord.Message.Should().Be("ERROR: No user with that email address was found");
                });
        }

        [Fact]
        public void InvalidPasswordUserLoginShouldReturnEmailMatchButNotPasswordMatch()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var newUserAdapter = IocHybridWrapper.GetInstance<AddNewUserUiAdapter>();
                    var userLoginAdapter = IocHybridWrapper.GetInstance<UserLoginUiAdapter>();

                    newUserAdapter.AddNewUser("user1", "Passw@rd123", "user1@test.com", "john", "one");

                    var userLoginRecord = userLoginAdapter.VerifyEmailAndPassword("user1@test.com", "Passw@rd1234");

                    userLoginRecord.UserLoggedIn.Should().BeFalse();
                    userLoginRecord.Message.Should().Be("ERROR: A matching email was found but the entered password was not correct");
                });
        }
    }
}
