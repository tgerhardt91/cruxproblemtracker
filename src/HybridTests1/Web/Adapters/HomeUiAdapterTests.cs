﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement;
using Core.ProblemManagement.Objects;
using FluentAssertions;
using HybridTests.DependancyResolution;
using Infrastructure;
using Infrastructure.Admin;
using Xunit;

namespace HybridTests.Web.Adapters
{
    public class HomeUiAdapterTests
    {
            [Fact]
            public void AdapterShouldReturnAllSetsOrderedByDate()
            {
                IocHybridWrapper.TestSetupWithCustomConfiguration(
                    container =>
                    { },
                    () =>
                    {
                        DatabaseCleaner.DeleteAllData();

                        var homeAdapter = IocHybridWrapper.GetInstance<HomeUiAdapter>();

                        var setRepository = IocHybridWrapper.GetInstance<ISetRepository>();

                        setRepository.InsertSet(new Set
                        {
                            SetId = Guid.NewGuid(),
                            SetDate = new DateTime(2017, 02, 15),
                            SetLocation = "East Corrall"
                        });
                        setRepository.InsertSet(new Set
                        {
                            SetId = Guid.NewGuid(),
                            SetDate = new DateTime(2017, 05, 15),
                            SetLocation = "West Corrall"
                        });
                        setRepository.InsertSet(new Set
                        {
                            SetId = Guid.NewGuid(),
                            SetDate = new DateTime(2017, 04, 15),
                            SetLocation = "West Corrall"
                        });

                        var setList = homeAdapter.GetSets();

                        setList.Count.Should().Be(3);
                        setList[0].SetDate.Should().BeAfter(setList[1].SetDate);
                        setList[1].SetDate.Should().BeAfter(setList[2].SetDate);
                    });
            }

        [Fact]
        public void AdapterShouldReturnProblemsFromSetBySetId()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var homeAdapter = IocHybridWrapper.GetInstance<HomeUiAdapter>();

                    var setRepository = IocHybridWrapper.GetInstance<ISetRepository>();

                    var set1 = new Set
                    {
                        SetId = Guid.NewGuid(),
                        SetDate = new DateTime(2017, 02, 15),
                        SetLocation = "East Corrall"
                    };

                    var set2 = new Set
                    {
                        SetId = Guid.NewGuid(),
                        SetDate = new DateTime(2017, 05, 15),
                        SetLocation = "West Corrall"
                    };

                    setRepository.InsertSet(set1);
                    setRepository.InsertSet(set2);

                    var problemRepository = IocHybridWrapper.GetInstance<IProblemRepository>();

                    var set1Problem1Id = Guid.NewGuid();
                    var set1Problem2Id = Guid.NewGuid();

                    var set2Problem1Id = Guid.NewGuid();
                    var set2Problem2Id = Guid.NewGuid();


                    problemRepository.InsertProblem(
                        new Problem
                        {
                            AssociatedSet = set1,
                            Color = "Red",
                            CommunityGrade = 7,
                            Grade = 8,
                            ProblemId = set1Problem1Id,
                            Setter = "Test Setter",
                            Tags = "#Crimpy#Easy#Fun"
                        });
                    problemRepository.InsertProblem(
                        new Problem
                        {
                            AssociatedSet = set1,
                            Color = "Green",
                            CommunityGrade = 5,
                            Grade = 6,
                            ProblemId = set1Problem2Id,
                            Setter = "Test Setter",
                            Tags = "#HeightDependent"
                        });
                    problemRepository.InsertProblem(
                        new Problem
                        {
                            AssociatedSet = set2,
                            Color = "Red",
                            CommunityGrade = 11,
                            Grade = 10,
                            ProblemId = set2Problem1Id,
                            Setter = "Test Setter",
                            Tags = "#Hard#Fun#Pumpy#Crimpy" 
                        });
                    problemRepository.InsertProblem(
                        new Problem
                        {
                            AssociatedSet = set2,
                            Color = "Red",
                            CommunityGrade = 4,
                            Grade = 3,
                            ProblemId = set2Problem2Id,
                            Setter = "Test Setter",
                            Tags = ""
                        });

                    var set1ProblemList = homeAdapter.GetProblemsFromSet(set1.SetId);

                    set1ProblemList.Count.Should().Be(2);

                    var set1ProblemIds = set1ProblemList.Select(s => s.ProblemId).ToList();
                    set1ProblemIds.Should().Contain(set1Problem1Id);
                    set1ProblemIds.Should().Contain(set1Problem2Id);

                    var set2ProblemList = homeAdapter.GetProblemsFromSet(set2.SetId);

                    set2ProblemList.Count.Should().Be(2);

                    var set2ProblemIds = set2ProblemList.Select(s => s.ProblemId).ToList();
                    set2ProblemIds.Should().Contain(set2Problem1Id);
                    set2ProblemIds.Should().Contain(set2Problem2Id);
                });
        }
    }
}
