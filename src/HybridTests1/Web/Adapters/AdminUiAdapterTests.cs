﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement.Objects;
using FluentAssertions;
using HybridTests.DependancyResolution;
using Infrastructure.Admin;
using Xunit;

namespace HybridTests
{
    public class AdminUiAdapterTests
    {
        [Fact]
        public void AdapterShouldReturnCorrectResponseOnSuccessfullSetAdd()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var adminAdapter = IocHybridWrapper.GetInstance<AdminUiAdapter>();

                    var response = adminAdapter.AddSet(new Set
                    {
                        SetId = Guid.NewGuid(),
                        SetDate = new DateTime(2017, 05, 15),
                        SetLocation = "West Corrall"
                    });

                    response.Message.Should().Be("Set Added");
                    response.SetAdded.Should().BeTrue();
                });
        }       

        [Fact]
        public void AdapterShouldReturnCorrectResponseOnUnsuccessfullSetAdd()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var adminAdapter = IocHybridWrapper.GetInstance<AdminUiAdapter>();

                    adminAdapter.AddSet(new Set
                    {
                        SetId = Guid.NewGuid(),
                        SetDate = new DateTime(2017, 05, 15),
                        SetLocation = "West Corrall"
                    });

                    var response = adminAdapter.AddSet(new Set
                    {
                        SetId = Guid.NewGuid(),
                        SetDate = new DateTime(2017, 05, 15),
                        SetLocation = "West Corrall"
                    });

                    response.SetAdded.Should().BeFalse();
                });
        }

        [Fact]
        public void AdapterShouldReturnCorrectResponseOnSuccessfullProblemAdd()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var adminAdapter = IocHybridWrapper.GetInstance<AdminUiAdapter>();

                    var setId = Guid.NewGuid();

                    var associatedSet = new Set
                    {
                        SetId = setId,
                        SetDate = new DateTime(2017, 05, 15),
                        SetLocation = "West Corrall"
                    };

                    var problemId = Guid.NewGuid();

                    adminAdapter.AddSet(associatedSet);

                    var response = adminAdapter.AddProblem(new Problem
                    {
                        AssociatedSet = associatedSet,
                        Color = "Red",
                        CommunityGrade = 7,
                        Grade = 8,
                        ProblemId = problemId,
                        Setter = "Test Setter"
                    });

                    response.ProblemAdded = true;
                    response.Message = "Problem Added";
                });
        }

        [Fact]
        public void AdapterShouldReturnCorrectResponseOnUnsuccessfullProblemAddBecauseOfInvalidSet()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var adminAdapter = IocHybridWrapper.GetInstance<AdminUiAdapter>();

                    var setId = Guid.NewGuid();

                    var associatedSet = new Set
                    {
                        SetId = setId,
                        SetDate = new DateTime(2017, 05, 15),
                        SetLocation = "West Corrall"
                    };

                    adminAdapter.AddSet(associatedSet);

                    var response = adminAdapter.AddProblem(new Problem
                    {
                        AssociatedSet = new Set
                        {
                            SetDate = new DateTime(2017, 03, 14),
                            SetId = Guid.NewGuid(),
                            SetLocation = "East Corrall"
                        },
                        Color = "Red",
                        CommunityGrade = 7,
                        Grade = 8,
                        ProblemId = Guid.NewGuid(),
                        Setter = "Test Setter"
                    });

                    response.ProblemAdded = false;
                });
        }

        [Fact]
        public void AdapterShouldReturnCorrectResponseOnUnsuccessfullProblemAddBecauseOfDuplicateProblem()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var adminAdapter = IocHybridWrapper.GetInstance<AdminUiAdapter>();

                    var setId = Guid.NewGuid();

                    var associatedSet = new Set
                    {
                        SetId = setId,
                        SetDate = new DateTime(2017, 05, 15),
                        SetLocation = "West Corrall"
                    };

                    var problemId = Guid.NewGuid();

                    adminAdapter.AddSet(associatedSet);

                    adminAdapter.AddProblem(new Problem
                    {
                        AssociatedSet = associatedSet,
                        Color = "Red",
                        CommunityGrade = 7,
                        Grade = 8,
                        ProblemId = problemId,
                        Setter = "Test Setter"
                    });

                    var response = adminAdapter.AddProblem(new Problem
                    {
                        AssociatedSet = associatedSet,
                        Color = "Red",
                        CommunityGrade = 7,
                        Grade = 8,
                        ProblemId = problemId,
                        Setter = "Test Setter"
                    });

                    response.ProblemAdded = false;
                });
        }
    }
}
