﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement;
using Core.ProblemManagement.Dtos;
using Database;
using FluentAssertions;
using HybridTests.DependancyResolution;
using Infrastructure;
using Xunit;

namespace HybridTests
{
    public class NavbarUiAdapterTests
    {
        [Fact]
        public void GetAllSetsShouldReturnAllSets()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                {},
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var setGateway = IocHybridWrapper.GetInstance<ISetGateway>();

                    var setId1 = Guid.NewGuid();
                    var setId2 = Guid.NewGuid();
                    var setId3 = Guid.NewGuid();

                    setGateway.InsertSetDto(new SetDto
                    {
                        SetId = setId1,
                        SetDate = new DateTime(2017, 03, 01),
                        SetLocation = "WestCorall"
                    });
                    setGateway.InsertSetDto(new SetDto
                    {
                        SetId = setId2,
                        SetDate = new DateTime(2017, 03, 07),
                        SetLocation = "WestCorall"
                    });
                    setGateway.InsertSetDto(new SetDto
                    {
                        SetId = setId3,
                        SetDate = new DateTime(2017, 05, 01),
                        SetLocation = "EastFuture"
                    });

                    var adapter = IocHybridWrapper.GetInstance<NavbarUiAdapter>();

                    var sets = adapter.GetAllSets();

                    sets.Count.Should().Be(3);
                    sets.Should().Contain(x => x.SetId == setId1);
                    sets.Should().Contain(x => x.SetId == setId2);
                    sets.Should().Contain(x => x.SetId == setId3);
                });
        }

        [Fact]
        public void GetMostRecentSetShouldReturnMostRecentSet()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var setGateway = IocHybridWrapper.GetInstance<ISetGateway>();

                    var setId1 = Guid.NewGuid();
                    var setId2 = Guid.NewGuid();
                    var setId3 = Guid.NewGuid();
                    var setId4 = Guid.NewGuid();

                    setGateway.InsertSetDto(new SetDto
                    {
                        SetId = setId1,
                        SetDate = new DateTime(2017, 03, 01),
                        SetLocation = "WestCorall"
                    });
                    setGateway.InsertSetDto(new SetDto
                    {
                        SetId = setId2,
                        SetDate = new DateTime(2017, 03, 07),
                        SetLocation = "WestCorall"
                    });
                    setGateway.InsertSetDto(new SetDto
                    {
                        SetId = setId3,
                        SetDate = new DateTime(2017, 05, 01),
                        SetLocation = "EastFuture"
                    });

                    var adapter = IocHybridWrapper.GetInstance<NavbarUiAdapter>();

                    var mostRecentSet = adapter.GetMostRecentSet();

                    mostRecentSet.SetId.Should().Be(setId3);

                    setGateway.InsertSetDto(new SetDto
                    {
                        SetId = setId4,
                        SetDate = new DateTime(2017, 07, 01),
                        SetLocation = "WestCorall"
                    });

                    mostRecentSet = adapter.GetMostRecentSet();
                    mostRecentSet.SetId.Should().Be(setId4);
                });
        }
    }
}
