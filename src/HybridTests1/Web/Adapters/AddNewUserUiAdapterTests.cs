﻿using System.Collections.Generic;
using Core.UserManagement;
using Core.UserManagement.Objects;
using FluentAssertions;
using HybridTests.DependancyResolution;
using Infrastructure.UserManagement;
using SharedTestDoubles;
using Xunit;

namespace HybridTests.Web.Adapters
{
    public class AddNewUserUiAdapterTests
    {
        [Fact]
        public void AddingValidNewUserShouldReturnCorrectAddNewUserRecord()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var adapter = IocHybridWrapper.GetInstance<AddNewUserUiAdapter>();

                    var addNewUserRecord = adapter.AddNewUser("user4", "Passw@rd123", "user4@test.com", "john", "four");

                    addNewUserRecord.NewUserAdded.Should().BeTrue();
                    addNewUserRecord.Message.Should().Be("Success");
                });
        }

        [Fact]
        public void AttemptingToAddExistingUserNameShouldReturnCorrectAddNewUserRecord()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var adapter = IocHybridWrapper.GetInstance<AddNewUserUiAdapter>();

                    adapter.AddNewUser("user2", "Passw@rd123", "user2@test.com", "john", "two");

                    var addNewUserRecord = adapter.AddNewUser("user2", "Passw@rd123", "user2@test.com", "john", "two");

                    addNewUserRecord.NewUserAdded.Should().BeFalse();
                    addNewUserRecord.Message.Should().Be("An account with that email address has already been created");

                    addNewUserRecord = adapter.AddNewUser("user2", "Passw@rd123", "user22@test.com", "john", "two");

                    addNewUserRecord.NewUserAdded.Should().BeFalse();
                    addNewUserRecord.Message.Should().Be("Username is already taken");
                });
        }

        [Fact]
        public void AddingNewUserWithInvalidPasswordShouldReturnCorrectAddNewUserRecord()
        {
            IocHybridWrapper.TestSetupWithCustomConfiguration(
                container =>
                { },
                () =>
                {
                    DatabaseCleaner.DeleteAllData();

                    var adapter = IocHybridWrapper.GetInstance<AddNewUserUiAdapter>();

                    var addNewUserRecord = adapter.AddNewUser("user40", "Pw0@)", "user40@test.com", "john", "four");

                    addNewUserRecord.NewUserAdded.Should().BeFalse();
                    addNewUserRecord.Message.Should().Be("Password must be at least 7 characters long\n");

                    addNewUserRecord = adapter.AddNewUser("user41", "Passwrd123", "user41@test.com", "john", "four");

                    addNewUserRecord.NewUserAdded.Should().BeFalse();
                    addNewUserRecord.Message.Should().Be("Password must contain at least 1 special character, 1 uppercase character and 1 number\n");

                    addNewUserRecord = adapter.AddNewUser("user42", "Pw123", "user42@test.com", "john", "four");

                    addNewUserRecord.NewUserAdded.Should().BeFalse();
                    addNewUserRecord.Message.Should().Be("Password must be at least 7 characters long\n" +
                                                         "Password must contain at least 1 special character, 1 uppercase character and 1 number\n");
                });
        }
    }
}
