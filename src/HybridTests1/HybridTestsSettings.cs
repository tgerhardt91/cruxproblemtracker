﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Settings;

namespace HybridTests
{
    public class HybridTestsSettings : IDatabaseSettings
    {
        public string ConnectionString
        {
            get
            {
                return ConfigurationManager.
                    ConnectionStrings["DatabaseConnectionString"].ConnectionString;
            }            
        }

        public string DatabaseName
        {
            get
            { return ConfigurationManager.
                    AppSettings["DatabaseName"];
            }
        }
    }
}
