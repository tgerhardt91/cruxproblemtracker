﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Settings;
using DependencyManager;
using StructureMap;
using StructureMap.Graph.Scanning;
using IContainer = StructureMap.IContainer;

namespace HybridTests.DependancyResolution
{
    public class IocHybridWrapper
    {
        private static IContainer _activeContainer;

        private static bool WasInit { get; set; }

        public static void TestSetupWithCustomConfiguration(
            Action<IContainer> testSetup,
            Action testAction)
        {
            if (WasInit == false)
            {
                Init();
            }

            var rootContainer = _activeContainer;

            try
            {
                using (_activeContainer = _activeContainer.GetNestedContainer())
                {
                    testSetup(_activeContainer);
                    testAction();
                }
            }
            finally
            {
                _activeContainer = rootContainer;
            }
        }

        public static void Init()
        {
            IContainer container = IocWrapper.Instance.IContainer.CreateChildContainer();
            _activeContainer = container;

            _activeContainer.Configure(scan =>
            {
                scan.For<IDatabaseSettings>().Use<HybridTestsSettings>();
            });

            WasInit = true;
        }

        public static TType GetInstance<TType>()
        {
            if (!WasInit)
            {
                Init();
            }

            return _activeContainer.GetInstance<TType>();
        }
    }
}
