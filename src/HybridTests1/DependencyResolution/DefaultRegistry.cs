﻿using Core.UserManagement;
using Microsoft.Win32;
using StructureMap;
using Registry = StructureMap.Registry;

namespace HybridTests.DependencyResolution
{
    public class DefaultRegistry : Registry
    {
        public DefaultRegistry()
        {
            Scan(scan =>
            {
                scan.WithDefaultConventions();
                scan.TheCallingAssembly();
            });
        }
    }
}
