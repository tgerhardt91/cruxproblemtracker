﻿using System;
using System.ComponentModel;
using StructureMap;
using IContainer = StructureMap.IContainer;

namespace DependencyManager
{
    public class IocWrapper
    {
        private static readonly object LockObject = new Object();
        private static IocWrapper _instance;
        private IContainer _container;

        public IContainer IContainer
        {
            get { return _container; }
        }

        public static IocWrapper Instance
        {
            get
            {
                lock (LockObject)
                {
                    if (_instance == null)
                    {
                        _instance = new IocWrapper();
                    }

                    return _instance;
                }
            }
        }

        public void ExecuteWithCustomContainer(
            Action<IContainer> custom,
            Action action)
        {
            var rootContainer = _container;

            try
            {
                using (var childContainer = _container.CreateChildContainer())
                {
                    custom(childContainer);
                    _container = childContainer;
                    action();
                }
            }
            finally
            {
                _container = rootContainer;
            }
        }

        private IocWrapper()
        {
            _container = new StructureMap.Container(assemblyScanner =>
            {
                assemblyScanner.Scan(x =>
                {
                    x.WithDefaultConventions();
                    x.TheCallingAssembly();
                    x.AssembliesFromApplicationBaseDirectory();

                    x.LookForRegistries();
                });
            });
        }

        public T GetInstance<T>()
        {
            return _container.GetInstance<T>();
        }

        public void For<TInterface, TConcrete>() where TConcrete : TInterface
        {
            _container.Configure(x => x.For<TInterface>().Use<TConcrete>());
        }

        public void Init()
        {
        }
    }
}
