﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement.Objects;
using FluentAssertions;
using Xunit;

namespace UnitTests
{
    public class ProblemDisplayDomainModelTests
    {
        private class TestHelper
        {
            public List<Problem> GetTestProblemSet()
            {
                var set1 = new Set
                {
                    SetId = Guid.NewGuid(),
                    SetDate = new DateTime(2017, 02, 15),
                    SetLocation = "East Corrall"
                };

                var set2 = new Set
                {
                    SetId = Guid.NewGuid(),
                    SetDate = new DateTime(2017, 05, 15),
                    SetLocation = "West Corrall"
                };

                var problem1Id = Guid.NewGuid();
                var problem2Id = Guid.NewGuid();
                var problem3Id = Guid.NewGuid();
                var problem4Id = Guid.NewGuid();

                var problems = new List<Problem>
                {
                    new Problem
                    {
                        AssociatedSet = set1,
                        Color = "Red",
                        CommunityGrade = 7,
                        Grade = 8,
                        ProblemId = problem1Id,
                        Setter = "Test Setter",
                        Tags = "#Crimpy#Easy#Fun"
                    },
                    new Problem
                    {
                        AssociatedSet = set1,
                        Color = "Green",
                        CommunityGrade = 5,
                        Grade = 6,
                        ProblemId = problem2Id,
                        Setter = "Test Setter",
                        Tags = "#HeightDependent#Crimpy"
                    },
                    new Problem
                    {
                        AssociatedSet = set2,
                        Color = "Red",
                        CommunityGrade = 11,
                        Grade = 10,
                        ProblemId = problem3Id,
                        Setter = "Test Setter",
                        Tags = "#Crimpy#Hard#Fun#Pumpy"
                    },
                    new Problem
                    {
                        AssociatedSet = set2,
                        Color = "Red",
                        CommunityGrade = 4,
                        Grade = 3,
                        ProblemId = problem4Id,
                        Setter = "Test Setter",
                        Tags = ""
                    }
                };

                return problems;
            }
        }

        [Fact]
        public void GetProblemsShouldReturnAllProblemsWhenNoFiltersExist()
        {
            var helper = new TestHelper();

            var problems = helper.GetTestProblemSet();

            var problemDisplayDomainModel = new ProblemDisplayDomainModel(problems);

            var filteredProblems = problemDisplayDomainModel.GetProblems();

            var filteredProblemIds = filteredProblems.Select(p => p.ProblemId).ToList();

            filteredProblems.Count.Should().Be(problems.Count);

            foreach (var problem in problems)
            {
                filteredProblemIds.Should().Contain(problem.ProblemId);
            }
        }

        [Fact]
        public void GetProblemsShouldReturnAllCorrectProblemsWhenTagFilterExists()
        {
            var helper = new TestHelper();

            var problems = helper.GetTestProblemSet();

            var problemDisplayDomainModel =
                new ProblemDisplayDomainModel(problems) {TagFilter = new List<string> {"Fun", "Crimpy"}};

            var filteredProblems = problemDisplayDomainModel.GetProblems();

            var filteredProblemIds = filteredProblems.Select(p => p.ProblemId).ToList();

            filteredProblems.Count.Should().Be(2);

            foreach (var problem in problems.Where(p => p.Tags.Contains("Crimpy") && p.Tags.Contains("Fun")))
            {
                filteredProblemIds.Should().Contain(problem.ProblemId);
            }
        }

        [Fact]
        public void GetUniqueAvailableTagsShouldAllTagsWithNoDupes()
        {
            var helper = new TestHelper();

            var problems = helper.GetTestProblemSet();

            var problemDisplayDomainModel = new ProblemDisplayDomainModel(problems);

            var tags = problemDisplayDomainModel.GetUniqueAvailiableTags();

            tags.Count.Should().Be(6);

            tags.Should().Contain("Crimpy");
            tags.Should().Contain("Easy");
            tags.Should().Contain("Fun");
            tags.Should().Contain("Hard");
            tags.Should().Contain("Pumpy");
            tags.Should().Contain("HeightDependent");
        }
    }
}
