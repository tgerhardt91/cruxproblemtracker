﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.UserManagement;
using Core.UserManagement.Objects;
using Infrastructure.UserManagement;
using FluentAssertions;
using AutoFixture;
using Xunit;

namespace UnitTests
{
    public class SecurePasswordHasherTests
    {
        [Fact]
        public void SecurePasswordHasherShouldNotFlagGoodMatchHashedPassword()
        {
            var testHelper = new SecurePasswordHasherTestHelper();

            var existingUsers = testHelper.CreateExistingUserList();

            var newUser = new UserLogin
            {
                UserId = Guid.NewGuid(),
                Email = "user1!",
                Password = "p@ssworD1"
            };

            var hasher = new SecurePasswordHasher();

            var response = hasher.Verify(newUser.Password, existingUsers.ElementAt(0).Password);

            response.Should().BeTrue();
        }

        [Fact]
        public void SecurePasswordHasherShouldFlagBadMatchHashedPassword()
        {
            var testHelper = new SecurePasswordHasherTestHelper();

            var existingUsers = testHelper.CreateExistingUserList();

            var newUser = new UserLogin
            {
                UserId = Guid.NewGuid(),
                Email = "user1!",
                Password = "password"
            };

            var hasher = new SecurePasswordHasher();

            var response = hasher.Verify(newUser.Password, existingUsers.ElementAt(0).Password);

            response.Should().BeFalse();
        }
    }

    public class SecurePasswordHasherTestHelper
    {
        public List<UserLogin> CreateExistingUserList()
        {
            var existingUsers = new List<UserLogin>();

            var testFixture = new Fixture();

            var hasher = new SecurePasswordHasher();

            var user1 = testFixture.Build<UserLogin>()
                .With(x => x.UserId, Guid.NewGuid())
                .With(x => x.Email, "user1@test.com")
                .With(x => x.Password, hasher.Hash("p@ssworD1"))
                .Create();
            existingUsers.Add(user1);
            var user2 = testFixture.Build<UserLogin>()
                .With(x => x.UserId, Guid.NewGuid())
                .With(x => x.Email, "user2@test.com")
                .With(x => x.Password, hasher.Hash("p@ssworD2"))
                .Create();
            existingUsers.Add(user2);
            var user3 = testFixture.Build<UserLogin>()
                .With(x => x.UserId, Guid.NewGuid())
                .With(x => x.Email, "user3@test.com")
                .With(x => x.Password, hasher.Hash("p@ssworD3"))
                .Create();
            existingUsers.Add(user3);

            return existingUsers;
        }
    }
}
