﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.UserManagement;
using Core.UserManagement.Objects;
using FluentAssertions;
using AutoFixture;
using Xunit;

namespace UnitTests
{
    public class NewUserValidatorTests
    {
        [Fact]
        public void NewUserValidatorShouldNotFlagValidNewUser()
        {
            var testHelper = new NewUserValidatorTestHelper();

            var existingUsers = testHelper.CreateExistingUserList();

            var validator = new NewUserValidator();

            var response = validator.ValidateNewUser("testUser123", "user123@test.com", "testPword123!", existingUsers);

            response.IsUserNameUnique.Should().BeTrue();
            response.DoesPasswordMeetMinimumComplexity.Should().BeTrue();
            response.DoesPasswordMeetMinimumLength.Should().BeTrue();
        }

        [Fact]
        public void NewUserValidatorShouldFlagExistingUserName()
        {
            var testHelper = new NewUserValidatorTestHelper();

            var existingUsers = testHelper.CreateExistingUserList();


            var validator = new NewUserValidator();

            var response = validator.ValidateNewUser(existingUsers.FirstOrDefault().UserName, "user123@test.com", "testPword123!", existingUsers);

            response.IsUserNameUnique.Should().BeFalse();
            response.DoesPasswordMeetMinimumComplexity.Should().BeTrue();
            response.DoesPasswordMeetMinimumLength.Should().BeTrue();
        }

        [Fact]
        public void NewUserValidatorShouldFlagPasswordThatDoesNotMeetLengthCriteria()
        {
            var testHelper = new NewUserValidatorTestHelper();

            var existingUsers = testHelper.CreateExistingUserList();

            var validator = new NewUserValidator();

            var response = validator.ValidateNewUser("testUser123", "user123@test.com", "Pwd1!", existingUsers);

            response.IsUserNameUnique.Should().BeTrue();
            response.DoesPasswordMeetMinimumComplexity.Should().BeTrue();
            response.DoesPasswordMeetMinimumLength.Should().BeFalse();
        }

        [Fact]
        public void NewUserValidatorShouldFlagPasswordThatDoesNotMeetMinimumComplexity()
        {
            var testHelper = new NewUserValidatorTestHelper();

            var existingUsers = testHelper.CreateExistingUserList();

            var validator = new NewUserValidator();

            var response = validator.ValidateNewUser("testUser123", "user123@test.com", "Password1", existingUsers);

            response.IsUserNameUnique.Should().BeTrue();
            response.DoesPasswordMeetMinimumComplexity.Should().BeFalse();
            response.DoesPasswordMeetMinimumLength.Should().BeTrue();

            response = validator.ValidateNewUser("testUser123", "user123@test.com", "Password!", existingUsers);

            response.IsUserNameUnique.Should().BeTrue();
            response.DoesPasswordMeetMinimumComplexity.Should().BeFalse();
            response.DoesPasswordMeetMinimumLength.Should().BeTrue();

            response = validator.ValidateNewUser("testUser123", "user123@test.com", "password1!", existingUsers);

            response.IsUserNameUnique.Should().BeTrue();
            response.DoesPasswordMeetMinimumComplexity.Should().BeFalse();
            response.DoesPasswordMeetMinimumLength.Should().BeTrue();
        }
    }

    public class NewUserValidatorTestHelper
    {
        public List<User> CreateExistingUserList()
        {
            var existingUsers = new List<User>();

            var testFixture = new Fixture();

            var user1 = testFixture.Build<User>()
                .With(x => x.UserId, Guid.NewGuid())
                .With(x => x.UserName, "user1")
                .With(x => x.Email, "user1@test.com")
                .With(x => x.IsAdmin, false)
                .With(x => x.FirstName, "Joe1")
                .With(x => x.LastName, "Smith1")
                .Create();
            existingUsers.Add(user1);
            var user2 = testFixture.Build<User>()
                .With(x => x.UserId, Guid.NewGuid())
                .With(x => x.UserName, "user2")
                .With(x => x.Email, "user2@test.com")
                .With(x => x.IsAdmin, false)
                .With(x => x.FirstName, "Joe2")
                .With(x => x.LastName, "Smith2")
                .Create();
            existingUsers.Add(user2);
            var user3 = testFixture.Build<User>()
                .With(x => x.UserId, Guid.NewGuid())
                .With(x => x.UserName, "user3")
                .With(x => x.Email, "user3@test.com")
                .With(x => x.IsAdmin, false)
                .With(x => x.FirstName, "Joe3")
                .With(x => x.LastName, "Smith3")
                .Create();
            existingUsers.Add(user3);

            return existingUsers;
        }
    }
}
