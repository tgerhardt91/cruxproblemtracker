﻿using System;
using System.Collections.Generic;
using Core.UserManagement;
using Core.UserManagement.Objects;

namespace SharedTestDoubles
{
    public class UserLoginGatewayFake : IUserLoginGateway
    {
        public List<UserLoginDto> _userLoginDtos = new List<UserLoginDto>();

        public void InsertUserLoginDto(UserLoginDto dto)
        {
            _userLoginDtos.Add(dto);
        }

        public IEnumerable<UserLoginDto> GetAllUserLogins()
        {
            return _userLoginDtos;
        }

        public UserLoginDto GetUserLoginByUserId(Guid userId)
        {
            throw new NotImplementedException();
        }

        public void DeleteAllRecords()
        {
            throw new NotImplementedException();
        }
    }
}
