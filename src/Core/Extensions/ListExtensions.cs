﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Extensions
{
    public static class ListExtensions
    {
        public static bool ContainsIgnoringCase(this List<string> list, string item)
        {
            foreach (var listItem in list)
            {
                if (string.Equals(listItem, item, StringComparison.OrdinalIgnoreCase))
                    return true;
            }

            return false;
        }
    }
}
