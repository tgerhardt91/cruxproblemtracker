﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Core.Extensions;

namespace Core.ProblemManagement.Objects
{
    public class ProblemDisplayDomainModel
    {
        private readonly List<Problem> _problems;

        public ProblemDisplayDomainModel(List<Problem> problems)
        {
            _problems = problems;
            TagFilter = new List<string>();
            UserFilter = null;
        }

        public List<string> TagFilter { get; set; }

        public Guid? UserFilter { get; set; }

        public List<Problem> GetProblems()
        {
            if (TagFilter.Any())
                return FilterByTag(_problems);

            return _problems;
        }

        public List<string> GetUniqueAvailiableTags()
        {
            var tags = new List<string>();

            foreach (var problem in _problems)
            {
                var tagList = TransformTagStringToList(problem.Tags);

                tags.AddRange(tagList);
            }

            return tags.Distinct().ToList();
        }

        private List<Problem> FilterByTag(IEnumerable<Problem> problems)
         {
            var filteredProblems = new List<Problem>();

            foreach (var problem in problems)
            {
                if(TagsMatch(TransformTagStringToList(problem.Tags), TagFilter))
                    filteredProblems.Add(problem);
            }

            return filteredProblems;
        }

        private static bool TagsMatch(List<string> problemTags, List<string> filterTags)
        {        
            var tagsMatch = true;

            if (problemTags.Count < filterTags.Count)
                return false;

            foreach (var tag in filterTags)
            {
                if (!problemTags.ContainsIgnoringCase(tag))
                    tagsMatch = false;
            }

            return tagsMatch;
        }

        private static List<string> TransformTagStringToList(string tags)
        {
            if (string.IsNullOrEmpty(tags))
                return new List<string>();

            tags = tags.Substring(1);

            const char splitSymbol = '#';

            return tags.Split(splitSymbol).ToList();
        }       
    }

    public class Problem
    {
        public Guid ProblemId { get; set; }
        public int Grade { get; set; }
        public int CommunityGrade { get; set; }
        public string Color { get; set; }
        public string Setter { get; set; }
        public Set AssociatedSet { get; set; }
        public string Tags { get; set; }
    }
}
