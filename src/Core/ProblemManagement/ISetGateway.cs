﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement.Dtos;

namespace Core.ProblemManagement
{
    public interface ISetGateway
    {
        void InsertSetDto(SetDto dto);
        SetDto[] GetAllSetDtos();
        void DeleteAllRecords();
    }
}
