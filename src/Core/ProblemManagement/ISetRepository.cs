﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement.Objects;

namespace Core.ProblemManagement
{
    public interface ISetRepository
    {
        void InsertSet(Set set);
        IEnumerable<Set> GetAllSets();
    }
}
