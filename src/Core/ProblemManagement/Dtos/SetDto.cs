﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ProblemManagement.Dtos
{
    public class SetDto
    {
        public Guid SetId { get; set; }
        public DateTime SetDate { get; set; }
        public string SetLocation { get; set; }
    }
}
