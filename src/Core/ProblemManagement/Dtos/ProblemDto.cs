﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Core.ProblemManagement
{
    public class ProblemDto
    {
        public Guid ProblemId { get; set; }
        public int Grade { get; set; }
        public int CommunityGrade { get; set; }
        public string Color { get; set; }
        public string Setter { get; set; }
        public Guid SetId { get; set; }
        public DateTime SetDate { get; set; }
        public string SetLocation { get; set; }
        public string Tags { get; set; }
    }
}
