﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ProblemManagement
{
    public interface IProblemGateway
    {
        void InsertProblemDto(ProblemDto dto);
        ProblemDto[] GetAllProblemDtos();
        ProblemDto[] GetBySetId(Guid setId);
        void DeleteAllRecords();
    }
}
