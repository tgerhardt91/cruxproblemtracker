﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement.Objects;

namespace Core.ProblemManagement
{
    public interface IProblemRepository
    {
        void InsertProblem(Problem problem);
        IEnumerable<Problem> GetAllProblems();
        IEnumerable<Problem> GetProblemsBySetId(Guid setId);
    }
}
