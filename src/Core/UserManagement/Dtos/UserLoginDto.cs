﻿using System;

namespace Core.UserManagement.Objects
{
    public class UserLoginDto
    {
        public Guid UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
