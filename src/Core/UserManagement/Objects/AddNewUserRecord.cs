﻿namespace Core.UserManagement.Objects
{
    public class AddNewUserRecord
    {
        public bool NewUserAdded { get; set; }
        public string Message { get; set; }
    }
}
