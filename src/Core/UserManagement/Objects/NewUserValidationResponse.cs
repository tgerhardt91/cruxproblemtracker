﻿namespace Core.UserManagement.Objects
{
    public class NewUserValidationResponse
    {
        public bool IsUserNameUnique { get; set; }
        public bool IsEmailUnique { get; set; }
        public bool DoesPasswordMeetMinimumLength { get; set; }
        public bool DoesPasswordMeetMinimumComplexity { get; set; }
    }
}
