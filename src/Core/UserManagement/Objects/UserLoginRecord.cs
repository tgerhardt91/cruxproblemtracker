﻿
namespace Core.UserManagement.Objects
{
    public class UserLoginRecord
    {
        public bool UserLoggedIn { get; set; }
        public string FirstName { get; set; }
        public string Message { get; set; }
    }
}
