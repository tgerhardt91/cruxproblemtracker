﻿using Core.UserManagement.Objects;
using System;
using System.Collections.Generic;

namespace Core.UserManagement
{
    public interface IUserRepository
    {
        void InsertNewUser(User newUser);
        User GetUserById(Guid userId);
        IEnumerable<User> GetAllUsers();
    }
}
