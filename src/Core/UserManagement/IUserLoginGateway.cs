﻿using System.Collections.Generic;
using Core.UserManagement.Objects;
using System;

namespace Core.UserManagement
{
    public interface IUserLoginGateway
    {
        void InsertUserLoginDto(UserLoginDto dto);
        IEnumerable<UserLoginDto> GetAllUserLogins();
        UserLoginDto GetUserLoginByUserId(Guid userId);
        void DeleteAllRecords();
    }
}
