﻿using Core.UserManagement.Dtos;
using System;
using System.Collections.Generic;

namespace Core.UserManagement
{
    public interface IUserGateway
    {
        void InsertNewUserDto(UserDto dto);
        IEnumerable<UserDto> GetAllUsers();
        UserDto GetUserById(Guid userId);
        void DeleteAllRecords();
    }
}
