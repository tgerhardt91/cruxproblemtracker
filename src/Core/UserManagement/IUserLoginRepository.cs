﻿using System.Collections.Generic;
using Core.UserManagement.Objects;
using System;

namespace Core.UserManagement
{
    public interface IUserLoginRepository
    {
        void InsertNewUserLogin(UserLogin newUser);
        UserLogin GetUserLoginById(Guid userId);
    }
}
