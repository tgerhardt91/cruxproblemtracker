﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.UserManagement.Objects;

namespace Core.UserManagement
{
    public class NewUserValidator
    {
        public NewUserValidationResponse ValidateNewUser(string userName, string email, string password, IEnumerable<User> existingUsers)
        {
            return new NewUserValidationResponse
            {
                IsUserNameUnique = DoesUserNameAlreadyExist(userName, existingUsers),
                IsEmailUnique = DoesEmailAlreadyExist(email, existingUsers),
                DoesPasswordMeetMinimumLength = DoesPasswordMeetMinimumLengthCriteria(password),
                DoesPasswordMeetMinimumComplexity = DoesPasswordMeetMinimumComplexityCriteria(password)
            };           
        }

        private static bool DoesUserNameAlreadyExist(string newUserName, IEnumerable<User> existingUsers)
        {
            return existingUsers.FirstOrDefault(x => x.UserName == newUserName) == null;
        }

        private static bool DoesEmailAlreadyExist(string newEmail, IEnumerable<User> existingUsers)
        {
            return existingUsers.FirstOrDefault(x => x.Email == newEmail) == null;
        }

        private static bool DoesPasswordMeetMinimumLengthCriteria(string password)
        {
            return password.Length >= 7;
        }

        private static bool DoesPasswordMeetMinimumComplexityCriteria(string password)
        {
            var meetsComplexityCriteria = !(
                    password.All(char.IsLetterOrDigit) 
                || !password.Any(char.IsDigit)
                || !password.Any(char.IsUpper));
            
            return meetsComplexityCriteria;
        }
    }
}
