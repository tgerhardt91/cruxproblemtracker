﻿using StructureMap;

namespace Core.DependencyResolution
{
    public class DefaultRegistry : Registry
    {
        public DefaultRegistry()
        {
            Scan(scan =>
            {
                scan.WithDefaultConventions();
                scan.TheCallingAssembly();
            });
        }
    }
}
