﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Admin
{
    public class AddProblemResponse
    {
        public bool ProblemAdded { get; set; }
        public string Message { get; set; }
    }
}
