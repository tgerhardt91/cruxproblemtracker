﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Admin
{
    public class AddSetResponse
    {
        public bool SetAdded { get; set; }
        public string Message { get; set; }
    }
}
