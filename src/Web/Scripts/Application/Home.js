﻿function ReloadPageWithNewSet() {
    var setId = $("#SelectedSetId option:selected").val();

    $("#problemTable").load("/Home/ProblemsInSet",
        {
            SetId: setId
        });
}