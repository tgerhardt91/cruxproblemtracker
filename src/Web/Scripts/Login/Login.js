﻿
function SubmitLogin() {
    var username = document.getElementById("loginUsernameInput").value;
    var password = document.getElementById("loginPasswordInput").value;

    $.post("/Login/Login",
        {
            model: {
                UserName: username,
                Password: password
            }
        },
        function () { UseLoggedInNavBar() });
}

function CloseLoginModal() {
    $("#loginModal").modal("hide");
}

function UseLoggedInNavBar() {
    $("#loginModal").modal("hide");
    $("#loginLink").hide();
    $("#navbarUserNameLink").show();
}

function EnableSubmitButton() {
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    if (username.length > 0 && password.length > 6) {
        $("#loginBtn").prop("disabled", false);
    } else {
        $("#loginBtn").prop("disabled", true);
    }
}