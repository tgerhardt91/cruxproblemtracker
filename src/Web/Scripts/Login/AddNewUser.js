﻿
function AddNewUser() {
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    $.post("/Login/AddNewUser",
    {
        model: {
            UserName: username,
            Password: password
        }
    });
}

function CloseNewUserModal() {
    $("#addNewUserModal").modal("hide");
}

function EnableSubmitButton() {
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    if (username.length > 0 && password.length > 6) {
        $("#submitNewUser").prop("disabled", false);
    } else {
        $("#submitNewUser").prop("disabled", true);
    }
}