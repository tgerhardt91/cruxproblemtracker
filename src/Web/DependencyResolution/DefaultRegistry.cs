﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Settings;
using Core.UserManagement;
using Database;
using Infrastructure;
using StructureMap;

namespace Web.DependencyResolution
{
    public class DefaultRegistry : Registry
    {
        public DefaultRegistry()
        {
            Scan(
                scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();

                    scan.With(new ControllerConvention());
                });

            For<IDatabaseSettings>().Use<Settings>();
        }
    }
}