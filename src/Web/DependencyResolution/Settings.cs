﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Core.Settings;

namespace Web.DependencyResolution
{
    public class Settings : IDatabaseSettings
    {
        public string ConnectionString
        {
            get
            {
                return ConfigurationManager.
                    ConnectionStrings["DatabaseConnectionString"].ConnectionString;
            }
        }

        public string DatabaseName
        {
            get
            {
                return ConfigurationManager.
                      AppSettings["DatabaseName"];
            }
        }
    }
}