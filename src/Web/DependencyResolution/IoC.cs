using StructureMap;
namespace Web {
    public static class IoC {
        public static IContainer Initialize()
        {
            return DependencyManager.IocWrapper.Instance.IContainer;        
        }   
    }
}