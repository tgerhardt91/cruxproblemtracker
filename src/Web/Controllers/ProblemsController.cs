﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.ProblemManagement.Objects;
using Infrastructure.ProblemManagement;
using Web.Models.Problems;

namespace Web.Controllers
{
    public class ProblemsController : Controller
    {
        private readonly ProblemRepository _problemRepository;
        private const string TempDataProblemsString = "problems";

        public ProblemsController(
            ProblemRepository problemRepository)
        {
            _problemRepository = problemRepository;
        }

        public ActionResult Problems()
        {
            var allProblems = _problemRepository.GetAllProblems().ToList();

            SetProblemsInTempData(allProblems);

            var problemDisplayDomainModel = new ProblemDisplayDomainModel(GetProblemsFromTempData());

            var model = new ProblemModel
            {
                FilteredProblems = problemDisplayDomainModel.GetProblems(),
                AvailiableTags = GetAvailiableTags(problemDisplayDomainModel)
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Problems(ProblemModel model)
        {
            var problemDisplayDomainModel = new ProblemDisplayDomainModel(GetProblemsFromTempData())
                    { TagFilter = model.SelectedTags};

            model.FilteredProblems = problemDisplayDomainModel.GetProblems();
            model.AvailiableTags = GetAvailiableTags(problemDisplayDomainModel);

            return View(model);
        }

        public List<SelectListItem> GetAvailiableTags(ProblemDisplayDomainModel domainModel)
        {
            var tags = domainModel.GetUniqueAvailiableTags();

            return tags.Select(t => new SelectListItem {Text = t, Value = t}).ToList();
        }

        public List<Problem> GetProblemsFromTempData()
        {
            var problems = new List<Problem>();

            if (TempData[TempDataProblemsString] != null)
            {
                problems = (List<Problem>)TempData[TempDataProblemsString];
                TempData.Keep();
            }                

            return problems;
        }

        public void SetProblemsInTempData(List<Problem> problems)
        {
            TempData[TempDataProblemsString] = problems;
        }
    }
}