﻿using Infrastructure.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models.Login;

namespace Web.Controllers
{
    public class AddNewUserController : Controller
    {
        private readonly AddNewUserUiAdapter _addNewUserUiAdapter;

        public AddNewUserController(AddNewUserUiAdapter addNewUserUiAdapter)
        {
            _addNewUserUiAdapter = addNewUserUiAdapter;
        }

        public ActionResult AddNewUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddNewUser(AddNewUserModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var addNewUserRecord = _addNewUserUiAdapter.AddNewUser(model.UserName, model.Password, model.Email, model.FirstName, model.LastName);

            if (addNewUserRecord.NewUserAdded)
            {
                return RedirectToAction("LoginAfterRegistration", "Login", new { email = model.Email, password = model.Password } );
            }
            else
                ModelState.AddModelError("", addNewUserRecord.Message);

            return View();
        }

        // GET: AddNewUser
        public ActionResult Index()
        {
            return View();
        }
    }
}