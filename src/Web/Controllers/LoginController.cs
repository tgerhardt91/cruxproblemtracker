﻿using Infrastructure.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Web.Models.Login;

namespace Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly UserLoginUiAdapter _loginUiAdapter;

        public LoginController(UserLoginUiAdapter loginUiAdapter)
        {
            _loginUiAdapter = loginUiAdapter;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var userLoginRecord = _loginUiAdapter.VerifyEmailAndPassword(model.Email, model.Password);

            if (userLoginRecord.UserLoggedIn)
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, userLoginRecord.FirstName));
                claims.Add(new Claim(ClaimTypes.Email, model.Email));

                var identity = new ClaimsIdentity(claims, "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);
                ViewBag.FirstName = userLoginRecord.FirstName;
            }
            else
            {
                ModelState.AddModelError("", "Hmmm either your email or password was invalid, please try again");
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult LoginAfterRegistration(String email, String password)
        {
            if (!ModelState.IsValid)
                return View();

            var userLoginRecord = _loginUiAdapter.VerifyEmailAndPassword(email, password);

            if (userLoginRecord.UserLoggedIn)
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, userLoginRecord.FirstName));
                claims.Add(new Claim(ClaimTypes.Email, email));

                var identity = new ClaimsIdentity(claims, "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);
                ViewBag.FirstName = userLoginRecord.FirstName;
            }
            else
            {
                ModelState.AddModelError("", "Hmmm either your email or password was invalid, please try again");
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout(LoginModel model)
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Index", "Home");
        }
    }
}