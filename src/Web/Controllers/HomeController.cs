﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models.Login;
using System.Security.Claims;
using Infrastructure;
using Web.Models.Home;
using Core.ProblemManagement.Objects;
using Web.Extensions;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly HomeUiAdapter _homeUiAdapter;

        public HomeController(
            HomeUiAdapter homeUiAdapter
        )
        {
            _homeUiAdapter = homeUiAdapter;
        }

        public ActionResult Index()
        {
            var setList = _homeUiAdapter.GetSets();

            var mostRecentSet = setList.FirstOrDefault();

            var problems = new List<Problem>();

            if (mostRecentSet != null)
                problems.AddRange(_homeUiAdapter.GetProblemsFromSet(mostRecentSet.SetId));

            var model = new HomeModel
            {
                SetList = setList,
                ProblemsInSet = problems
            };

            return View(model);
        }

        public ActionResult ProblemsInSet(
            string setId)
        {
            var problemsInSet = _homeUiAdapter.GetProblemsFromSet(new Guid(setId));

            return PartialView("ProblemsInSetPartial", problemsInSet);
        }
    }
}