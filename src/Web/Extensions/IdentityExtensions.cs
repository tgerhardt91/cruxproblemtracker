﻿using System.Security.Claims;
using System.Security.Principal;

namespace Web.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetName(this IIdentity identity)
        {
            var claimIdentity = identity as ClaimsIdentity;

            var claim = claimIdentity?.FindFirst(ClaimTypes.Name);

            return claim != null ? claim.Value : string.Empty;
        }

        public static string GetEmail(this IIdentity identity)
        {
            var claimIdentity = identity as ClaimsIdentity;

            var claim = claimIdentity?.FindFirst(ClaimTypes.Email);

            return claim!= null ? claim.Value : string.Empty;
        }
    }
}