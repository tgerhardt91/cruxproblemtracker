﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models.Login
{
    public class AddNewUserModel
    {
        [Required(ErrorMessage = "* Required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "* Required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "* Required")]
        [Display(Name = "Username (What other users will see)")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "* Required")]
        [Display(Name = "Email (Will be used for login)")]
        public string Email { get; set; }

        [Required(ErrorMessage = "* Required")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "* Required")]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Message { get; set; }
    }
}