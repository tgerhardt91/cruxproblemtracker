﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.ProblemManagement.Objects;

namespace Web.Models.Problems
{
    public class ProblemModel
    {
        public ProblemModel()
        {            
            FilteredProblems = new List<Problem>();
            SelectedTags = new List<string>();
            AvailiableTags = new List<SelectListItem>();
        }

        public List<Problem> FilteredProblems { get; set; }
        public List<string> SelectedTags { get; set; }
        public List<SelectListItem> AvailiableTags { get; set; }
    }
}