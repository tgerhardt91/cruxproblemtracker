﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.ProblemManagement.Objects;

namespace Web.Models.Home
{
    public class HomeModel
    {
        public  IEnumerable<Set> SetList { get; set; }

        public IEnumerable<SelectListItem> Sets
        {
            get
            {
                var allSets = SetList.Select(s => new SelectListItem
                {
                    Value = s.SetId.ToString(),
                    Text = s.SetDate.ToShortDateString() + " (" + s.SetLocation + ")"
                });

                return allSets;
            }
        }

        public string SelectedSetId { get; set; }

        public IEnumerable<Problem> ProblemsInSet { get; set; }
    }
}