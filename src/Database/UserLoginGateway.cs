﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Core.Settings;
using Core.UserManagement;
using Core.UserManagement.Objects;
using Dapper;
using System;

namespace Database
{
    public class UserLoginGateway : IUserLoginGateway
    {
        private readonly IDatabaseSettings _databaseSettings;

        public UserLoginGateway(
            IDatabaseSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }

        public void InsertUserLoginDto(UserLoginDto dto)
        {
            using (var db = DbConnection())
            {
                db.Execute(
                    "INSERT INTO [UserLogin] (UserId, Email, Password) " +
                    "VALUES (@UserId, @Email, @Password)",
                    new
                    {
                        dto.UserId,
                        dto.Email,
                        dto.Password
                    });
            }
        }

        public IEnumerable<UserLoginDto> GetAllUserLogins()
        {
            using (var db = DbConnection())
            {
                return db.Query<UserLoginDto>
                    ("SELECT * FROM [UserLogin]").ToArray();
            }
        }

        public UserLoginDto GetUserLoginByUserId(Guid userId)
        {
            using (var db = DbConnection())
            {
                return db.Query<UserLoginDto>
                    ("SELECT * FROM [UserLogin] WHERE UserId = @UserId", new { userId }).ToArray().FirstOrDefault();
            }
        }

        public void DeleteAllRecords()
        {
            using (var db = DbConnection())
            {
                db.Execute("DELETE FROM [UserLogin]");
            }
        }

        private IDbConnection DbConnection()
        {
            return new SqlConnection(_databaseSettings.ConnectionString);
        }
    }
}
