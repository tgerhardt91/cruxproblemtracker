﻿using System;
using System.Data;
using System.Data.SqlClient;
using Core.UserManagement;
using Core.Settings;
using Dapper;
using Core.UserManagement.Dtos;
using System.Collections.Generic;
using System.Linq;

namespace Database
{
    public class UserGateway : IUserGateway
    {
        private readonly IDatabaseSettings _databaseSettings;

        public UserGateway(
            IDatabaseSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }

        public void InsertNewUserDto(UserDto dto)
        {
            using (var db = DbConnection())
            {
                db.Execute(
                    "INSERT INTO [User] (UserId, UserName, Email, IsAdmin, FirstName, LastName) " +
                    "VALUES (@UserId, @UserName, @Email, @IsAdmin, @FirstName, @LastName)",
                    new
                    {
                        dto.UserId,
                        dto.UserName,
                        dto.Email,
                        dto.IsAdmin,
                        dto.FirstName,
                        dto.LastName
                    });
            }
        }

        public IEnumerable<UserDto> GetAllUsers()
        {
            using (var db = DbConnection())
            {
                return db.Query<UserDto>
                    ("SELECT * FROM [User]").ToArray();
            }
        }

        public UserDto GetUserById(Guid userId)
        {
            using (var db = DbConnection())
            {
                return db.Query<UserDto>
                    ("SELECT * FROM [User] WHERE UserId = @UserID", new { userId } ).ToArray().FirstOrDefault();
            }
        }

        public void DeleteAllRecords()
        {
            using (var db = DbConnection())
            {
                db.Execute("DELETE FROM [User]");
            }
        }

        private IDbConnection DbConnection()
        {
            return new SqlConnection(_databaseSettings.ConnectionString);
        }
    }
}
