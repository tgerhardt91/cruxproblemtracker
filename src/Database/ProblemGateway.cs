﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Core.ProblemManagement;
using Core.Settings;
using Dapper;

namespace Database
{
    public class ProblemGateway : IProblemGateway
    {
        private readonly IDatabaseSettings _databaseSettings;

        public ProblemGateway(
            IDatabaseSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }

        public void InsertProblemDto(ProblemDto dto)
        {
            using (var db = DbConnection())
            {
                db.Execute(
                    "INSERT INTO Problem (Grade, CommunityGrade, Color, Setter, ProblemId, SetId, SetDate, SetLocation, Tags) " +
                    "VALUES (@Grade, @CommunityGrade, @Color, @Setter, @ProblemId, @SetId, @SetDate, @SetLocation, @Tags)",
                    new
                    {
                        dto.Grade,
                        dto.CommunityGrade,
                        dto.Color, 
                        dto.Setter,
                        dto.ProblemId,
                        dto.SetId,
                        dto.SetDate,
                        dto.SetLocation,
                        dto.Tags
                    });
            }
        }

        public ProblemDto[] GetAllProblemDtos()
        {
            using (var db = DbConnection())
            {
                return db.Query<ProblemDto>
                    ("SELECT * FROM Problem").ToArray();
            }
        }

        public ProblemDto[] GetBySetId(Guid setId)
        {
            using (var db = DbConnection())
            {
                return db.Query<ProblemDto>
                    ("SELECT * FROM Problem WHERE SetId = @SetId", new { setId }).ToArray();
            }
        }

        public void DeleteAllRecords()
        {
            using (var db = DbConnection())
            {
                db.Execute("DELETE FROM Problem");
            }
        }

        private IDbConnection DbConnection()
        {
            return new SqlConnection(_databaseSettings.ConnectionString);
        }
    }
}
