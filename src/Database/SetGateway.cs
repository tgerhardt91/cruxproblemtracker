﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.ProblemManagement;
using Core.ProblemManagement.Dtos;
using Core.Settings;
using Dapper;

namespace Database
{
    public class SetGateway : ISetGateway
    {
        private readonly IDatabaseSettings _databaseSettings;

        public SetGateway(
            IDatabaseSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }

        public void InsertSetDto(SetDto dto)
        {
            using (var db = DbConnection())
            {
                db.Execute(
                    "INSERT INTO ProblemSet (SetId, SetDate, SetLocation) VALUES (@SetId, @SetDate, @SetLocation)",
                    new
                    {
                        dto.SetId,
                        dto.SetDate,
                        dto.SetLocation
                    });
            }
        }

        public SetDto[] GetAllSetDtos()
        {
            using (var db = DbConnection())
            {
                return db.Query<SetDto>
                    ("SELECT * FROM ProblemSet").ToArray();
            }
        }       

        public void DeleteAllRecords()
        {
            using (var db = DbConnection())
            {
                db.Execute("DELETE FROM ProblemSet");
            }
        }

        private IDbConnection DbConnection()
        {
            return new SqlConnection(_databaseSettings.ConnectionString);
        }
    }
}
