ALTER TABLE Problem DROP CONSTRAINT FK_ProblemSet_ProblemSetId

GO

ALTER TABLE ProblemSet DROP CONSTRAINT PK_SetId

GO

ALTER TABLE ProblemSet
DROP COLUMN ProblemSetId 

GO

ALTER TABLE ProblemSet
ADD SetId uniqueidentifier NOT NULL

GO

ALTER TABLE ProblemSet
ADD CONSTRAINT PK_SetId PRIMARY KEY
(SetId, SetDate, SetLocation)

GO
