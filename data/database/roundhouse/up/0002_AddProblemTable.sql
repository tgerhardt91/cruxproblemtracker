CREATE TABLE [Problem](
	[ProblemId] [int] IDENTITY(1,1) NOT NULL,
	[SetId] [int] NOT NULL,
	[Grade] [int] NOT NULL,
	[CommunityGrade] [int] NOT NULL,
	[Color] [nvarchar] (255) NOT NULL,
	[Setter] [nvarchar] (255) NULL,
	[Tags] [nvarchar] (1000) NULL,
CONSTRAINT [PK_ProblemId] PRIMARY KEY
(
	[ProblemId] ASC
)
)

GO

ALTER TABLE [Problem] WITH CHECK ADD CONSTRAINT
[FK_ProblemSet_ProblemSetId] FOREIGN KEY([SetId])
REFERENCES [ProblemSet] ([SetId])
