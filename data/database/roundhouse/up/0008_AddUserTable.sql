CREATE TABLE [User](
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [NVARCHAR] (255) NOT NULL,
    [Email] [NVARCHAR] (255) NOT NULL,
    [IsAdmin] [BIT] NOT NULL,
    [FirstName] [NVARCHAR] (255) NOT NULL,
    [LastName] [NVARCHAR] (255) NOT NULL,
CONSTRAINT [PK_UserName] PRIMARY KEY
(
[UserName] ASC
)
)