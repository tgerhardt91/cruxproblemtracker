ALTER TABLE ProblemSet
DROP CONSTRAINT PK_SetId

GO

ALTER TABLE ProblemSet
ADD CONSTRAINT PK_SetId PRIMARY KEY CLUSTERED 
(SetDate, SetLocation)

GO

ALTER TABLE Problem 
DROP CONSTRAINT PK_ProblemId

GO 

ALTER TABLE Problem 
DROP COLUMN ProblemId, SetId

GO

ALTER TABLE Problem 
ADD ProblemId uniqueidentifier NOT NULL,
SetId uniqueidentifier NOT NULL,
SetDate datetime NOT NULL,
SetLocation nvarchar(255) NOT NULL

GO

ALTER TABLE Problem
ADD CONSTRAINT PK_ProblemId PRIMARY KEY
(ProblemId)

GO

ALTER TABLE Problem
WITH CHECK ADD CONSTRAINT [FK_Set_Location_Date] 
FOREIGN KEY([SetDate], [SetLocation])
REFERENCES [ProblemSet] ([SetDate], [SetLocation])
