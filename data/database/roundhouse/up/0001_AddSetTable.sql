CREATE TABLE [ProblemSet](
	[SetId] [int] IDENTITY(1,1) NOT NULL,
	[SetDate] [datetime] NOT NULL,
	[SetLocation] [NVARCHAR] (255) NOT NULL,
CONSTRAINT [PK_SetId] PRIMARY KEY
(
[SetId] ASC
)
)