# README 

# Table of contents 
1. Git Notes
1. Git Branching Strategy
1. Deployment Tools
1. Database
1. Links

# Topics

## Git Notes 

* Walkthrough for SSH cloning (how to set up keys etc.) (https://stackoverflow.com/questions/7053608/how-to-use-git-extension-with-bitbucket-repository) 
* GitExtensions will hang when attempting to push using HTTPS. To fix the issue, force GitExtensions to prompt for authentication [(Link)](https://gist.github.com/mikemichaelis/1565603)

## Git Branching Strategy 

* [Atlassian Tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows)
* [Branching Diagram][BranchingDiagram]

[BranchingDiagram]:http://nvie.com/img/git-model@2x.png

## Deployment Tools 

* Combination of Jenkins and Docker? [(Link)](https://goto.docker.com/continuous-integration-pipeline.html)

## Database 

* Versioning/Change management tool: RoundhousE
* Can use script in "up" folder to insert test data when db is build locally (see VehiclePortfolioMaster for example)

## Links 

* jquery .load explanation [(Link)](http://api.jquery.com/load/)
* Task tracking tool [(Link)](https://trello.com/b/paM60es0)